<?php
/**
 * @file
 * Fimnet OpenID Connect client.
 */

class OpenIDConnectClientFimnet extends OpenIDConnectClientBase {

  /**
   * Overrides OpenIDConnectClientBase::settingsForm().
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    $form['api_base_url'] = array(
      '#title' => t('API Base url'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('api_base_url'),
    );
    $form['auth_base_url'] = array(
      '#title' => t('Auth Base url'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('auth_base_url'),
    );
    return $form;
  }

  /**
   * Overrides OpenIDConnectClientBase::getEndpoints().
   */
  public function getEndpoints() {
    $auth_base_url = rtrim($this->getSetting('auth_base_url'));

    $api = new FireApiClient(array(
      'auth_base_url' => $auth_base_url,
    ));
    return array(
      'authorization' => $auth_base_url . $api::AUTHORIZE_ENDPOINT,
      'token' => $auth_base_url . $api::TOKEN_ENDPOINT,
    );
  }

  /**
   * Implements OpenIDConnectClientInterface::decodeIdToken().
   */
  public function decodeIdToken($id_token) {
    list($headerb64, $claims64, $signatureb64) = explode('.', $id_token);
    $claims64 = str_replace(array('-', '_'), array('+', '/'), $claims64);
    $claims64 = base64_decode($claims64);
    // Store tokens for FireApi.
    $this->tokens = drupal_json_decode($claims64);

    return $this->tokens;
  }

  /**
   * Implements OpenIDConnectClientInterface::retrieveUserInfo().
   */
  public function retrieveUserInfo($access_token) {
    $endpoints = $this->getEndpoints();

    $api = new FireApiClient(array(
      'base_url' => $this->getSetting('api_base_url'),
      'auth_base_url' => $this->getSetting('auth_base_url'),
      'client_id' => $this->getSetting('client_id'),
      'client_secret' => $this->getSetting('client_secret'),
      'access_token' => $access_token,
    ));

    if (empty($this->tokens['sub'])) {
      return;
    }
    try {
      $response = $api->get('/users/' . $this->tokens['sub']);

      $email = sprintf('%s@%s', $response->fimnet_id, $this->getSetting('client_id'));

      if (!empty($response->fimnet_email) && valid_email_address($response->fimnet_email)) {
        $email = $response->fimnet_email;
      }
      return array(
        'email' => $email,
      );
    }
    catch (Exception $e) {
      openid_connect_log_request_error(__FUNCTION__, $this->name, $response);
      return FALSE;
    }
  }
}
