<?php

/**
 * @file
 * Provides Fimnet OpenID Connect client plugin.
 */

$plugin = array(
  'title' => t('Fimnet'),
  'class' => 'OpenIDConnectClientFimnet',
);
