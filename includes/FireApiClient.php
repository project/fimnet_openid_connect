<?php

/**
 * Fire API Client.
 */
class FireApiClient
{
	const AUTHORIZE_ENDPOINT = '/authorize';
	const TOKEN_ENDPOINT = '/token';
	const END_SESSION_ENDPOINT = '/logout';
	const BASE_URL = 'https://api.fimnet.fi';
	const AUTH_BASE_URL = 'https://auth.fimnet.fi';
	const PUBLIC_KEY = 
"-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxo3km+iiqmb3rrLOCpn8uxnoQ
8NgPfB5lswyqbRK3TIv7EGoBGl495W87avePuf5mmYaRoOjQREpb0A26g5rAHHbr
DCGT+sVy+i85tG3rNv0KRlzsRY5pIkg+Z4TQzE3XF3mNiyCGuNDZCmHHZnbOg+TE
KLuQtIqgNEVpS0Aj/QIDAQAB
-----END PUBLIC KEY-----";

	/**
	 * @var array stores the config option
	 */
	protected $config = array();

	/**
	 * @var array options for curl
	 */
	protected $curl_opts = array(
		CURLOPT_USERAGENT => 'FireApiClient/0.9.2',
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_SSL_VERIFYPEER => 1
	);

	/**
	 * @var array Last response
	 */
	protected $last_response;

	/**
	 * @var array Last request
	 */
	protected $last_request;

	/**
	 * @var array Last headers
	 */
	protected $last_headers;

	/**
	 * Constructor.
	 * @param $config array An assoc array of options
	 * @see SetConfig
	 */
	public function __construct(array $config = array())
	{
		$this->setConfig($config);
	}

	/**
	 * Set the config options.
	 *
	 * @param $config array An assoc array of options
	 * @return $this
	 */
	public function setConfig(array $config)
	{
		$this->config = array_merge(array(
			'access_token' => null,
			'client_id' => null,
			'client_secret' => null,

			'base_url' => self::BASE_URL,
			'auth_base_url' => self::AUTH_BASE_URL,
			'public_key' => self::PUBLIC_KEY,
			'http_auth' => false,
			'throw_exceptions' => true,
			'decode_json' => true,
			'debug' => false,

			'multipart' => false
		), $config);

		$this->config['base_url'] = rtrim($this->config['base_url'],'/');
		if ( ! $this->config['base_url'] ) {
			throw new InvalidArgumentException('Base URL cannot be empty');
		}

		$this->config['auth_base_url'] = rtrim($this->config['auth_base_url'],'/');
		if ( ! $this->config['auth_base_url'] ) {
			throw new InvalidArgumentException('Auth base URL cannot be empty');
		}

		// add the client_id to the user agent to make it easier for debugging
		$this->curl_opts[CURLOPT_USERAGENT] .= ' '.$this->config['client_id'];

		// callback to process the headers
		$this->curl_opts[CURLOPT_HEADERFUNCTION] = array($this, 'processHeader');

		return $this;
	}

	/**
	 * Shortcut to set config option
	 */
	public function __set($name, $value)
	{
		$this->config[$name] = $value;
	}

	/**
	 * Shortcut to get a config option
	 */
	public function __get($name)
	{
		return array_key_exists($name, $this->config) ? $this->config[$name] : null;
	}

	/**
	 * Use to change cURL options such as :
	 * CURLOPT_SSL_VERIFYPEER 0/1 wether or not to verify the validity of the SSL cert (should always be 1)
	 * CURLOPT_CONNECTTIMEOUT_MS
	 * CURLOPT_TIMEOUT_MS
	 * etc.
	 *
	 * @param $options array
	 * @return $this
	 */
	public function setCurlOptions(array $options)
	{
		$this->curl_opts = $options + $this->curl_opts;
		return $this;
	}

// OAuth2

	/**
	 * Return the authorization URL with the given parameters
	 * @param $opt array
	 * @return string (url)
	 */
	public function getAuthorizeUrl(array $opt = array())
	{
		$opt = array_merge(array(
			'state' => '',
			'redirect_uri' => '',
			'scope' => 'openid'
		), $opt);

		$qs = array(
			'response_type' => 'code',
			'client_id' => $this->config['client_id'],
			'state' => $opt['state'],
			'redirect_uri' => $opt['redirect_uri'],
			'scope' => is_array($opt['scope']) ? implode(' ',$opt['scope']) : $opt['scope']
		);

		return $this->config['auth_base_url'].self::AUTHORIZE_ENDPOINT.'?'.http_build_query($qs);
	}

	/**
	 * Exchange an authorization code for an access token.
	 * @param array $opt
	 * @return stdClass
	 */
	public function getAccessToken(array $opt = array())
	{
		$opt = array_merge(array(
			'code' => '',
			'redirect_uri' => '',

			'decrypt_id_token' => true
		), $opt);

		$response = $this->request('POST', self::TOKEN_ENDPOINT, array(
			'grant_type' => 'authorization_code',
			'code' => $opt['code'],
			'redirect_uri' => $opt['redirect_uri']
		), array(
			'http_auth' => 'basic',
			'base_url' => $this->config['auth_base_url']
		));

		if ( isset($response->error) ) {
			throw new FireApiClient_OAuthError(
				$response->error,
				$this->last_response['meta']['http_code'],
				$response->error_description
			);
		}

		if ( $opt['decrypt_id_token'] && isset($response->id_token) ) {
			$response->id_token = $this->decryptIdToken($response->id_token);
		}

		if ( ! $this->config['access_token'] ) {
			$this->config['access_token'] = $response->access_token;
		}

		return $response;
	}

	/**
	 * Decrypt an check signature of an ID Token (Open ID Connect)
	 *
	 * You shouldn't have to use it directly.
	 * @param $id_token string
	 * @return stdClass
	 */
	public function decryptIdToken($id_token)
	{
		$separator = '.';

		$array = explode($separator, $id_token);
		if ( sizeof($array) != 3 ) {
			throw new FireApiClient_Exception("Incorrect token format");
		}

		list($header, $payload, $signature) = explode($separator, $id_token);

		// the signature uses a "url-safe" base64 encoding, so we need to do a bit of replacement to decode it
		$decoded_signature = base64_decode(str_replace(array('-', '_'),array('+', '/'), $signature));

		// The header and payload are signed together
		$payload_to_verify = utf8_decode($header . $separator . $payload);

		// $header = base64_decode($header);

		$verified = openssl_verify($payload_to_verify, $decoded_signature, $this->config['public_key'], defined('OPENSSL_ALGO_SHA256') ? OPENSSL_ALGO_SHA256 : 'sha256');

		if ($verified !== 1) {
			throw new FireApiClient_Exception("Invalid signature");
		}

		// output the Crypto Token payload
		$id_token = json_decode(base64_decode($payload));

		// see http://openid.net/specs/openid-connect-core-1_0.html#rfc.section.3.1.3.7
		if ( $id_token->aud != $this->config['client_id'] ) {
			throw new FireApiClient_Exception(sprintf(
				"The token audience (%s) doesn't match the client id (%s)",
				$id_token->aud,
				$this->config['client_id']
			));
		}

		if ( $id_token->exp < time() ) {
			throw new FireApiClient_Exception("The ID token is expired");
		}

		return $id_token;
	}

	/**
	 * Return the logout URL
	 * @param $opt array
	 * @return string (url)
	 */
	public function getLogoutUrl(array $opt = array())
	{
		$opt = array_merge(array(
			'post_logout_redirect_uri' => null
		), $opt);

		$qs = array();
		if ( $opt['post_logout_redirect_uri'] ) {
			$qs['post_logout_redirect_uri'] = $opt['post_logout_redirect_uri'];
		}
		$qs = http_build_query($qs);
		return $this->config['auth_base_url'].self::END_SESSION_ENDPOINT.($qs ? '?'.$qs : '');
	}

// REST API

	/**
	 * Workaround for a "feature" of http_build_query. We need to keep empty
	 * arrays.
	 */
	static public function replaceEmptyArrays(&$value)
	{
		if ( is_array($value) ) {
			if ( empty($value) ) {
				$value = '';
			}
			else {
				array_walk($value, array(__CLASS__,'replaceEmptyArrays'));
			}
		}
	}

	/**
	 * Prepare the data.
	 *
	 * For some reason, curl doesn't work well with nested arrays. So we "flatten"
	 * the array like so:
	 * Array(
	 *   [crop] => Array(
	 *     [x] => 1
	 *     [y] => 1
	 *   )
	 * )
	 * becomes:
	 * Array(
	 *   [crop[x]] => 1
	 *   [crop[y]] => 2
	 * )
	 *
	 * Note: We cannot use http_build_query because in order to upload files,
	 * $data needs to be an array and contains "@/path/to/file" type of variables.
	 *
	 * @param  array|string $data
	 * @return array|string
	 */
	protected function prepData($data, $method = 'POST', array $opt = array())
	{
		$opt = array_merge($this->config, $opt);


		if ( ! is_array($data) ) {
			return $data;
		}

		if ( !($method == 'POST' && $opt['multipart'] == true) ) {
			self::replaceEmptyArrays($data);
			return empty($data) ? '' : http_build_query($data);
		}

		$flatten_data = array();
		foreach ( $data as $name => $value ) {
			if ( is_array($value) ) {
				foreach ( $value as $name2 => $value2 ) {
					$flatten_data[$name.'['.$name2.']'] = $value2;
				}
			}
			else {
				$flatten_data[$name] = $value;
			}
		}
		return $flatten_data;
	}

	/**
	 * Do a request.
	 * This method can be used directly, but you can also use the shortcuts
	 * get(), post(), put() and delete().
	 * @param string  $method  the HTTP method
	 * @param string  $url     the endpoint
	 * @param array   $data    data to post/put
	 */
	public function request($method, $url, array $data = array(), array $opt = array())
	{
		$opt = array_merge($this->config, $opt);

		$start_time = microtime(true);

		$this->last_headers = array();
		$this->last_response = array();

		$url = $opt['base_url'].'/'.ltrim($url,'/');

		$postfields = $this->prepData($data, $method, $opt);

		$this->last_request = array(
			'method' => $method,
			'url' => $url,
			'data' => $postfields,
			'auth' => null
		);

		$curl = curl_init();

		curl_setopt_array($curl, $this->curl_opts);

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);

		if ( $opt['http_auth'] ) {
			curl_setopt($curl, CURLOPT_HTTPAUTH, constant('CURLAUTH_' . strtoupper($opt['http_auth'])));
			curl_setopt($curl, CURLOPT_USERPWD, $opt['client_id'].":".$opt['client_secret']);
			$this->last_request['auth'] = 'HTTP '.strtoupper($opt['http_auth']).' Auth '.$opt['client_id'].":".$opt['client_secret'];
		} elseif ( $opt['access_token'] ) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Authorization: Bearer '.$opt['access_token']
			));
			$this->last_request['auth'] = 'Bearer '.$opt['access_token'];
		}


		$this->last_response['body'] = curl_exec($curl);
		if ( $this->last_response['body'] === false  && $opt['throw_exceptions'] ) {
			throw new FireApiClient_Exception('curl_exec failed (error: '. curl_error($curl).')');
		}

		$this->last_response["meta"] = curl_getinfo($curl);
		if ( $this->last_response["meta"] === false  && $opt['throw_exceptions'] ) {
			throw new FireApiClient_Exception('curl_getinfo failed');
		}

		$status_code = $this->last_response['meta']['http_code'];
		if ( $status_code != 200 && $opt['throw_exceptions'] ) {
			$response = json_decode($this->last_response["body"]);
			if ( ! $response || ! is_object($response) ) {
				throw new FireApiClient_Exception('API didn\'t return valid JSON on error');
			}

			if ( $status_code == 422 ) {
				throw new FireApiClient_ValidationError(
					$response->message,
					$status_code,
					isset($response->errors) ? $response->errors : array()
				);
			} elseif ( isset($response->error_description) ) { 
				throw new FireApiClient_OAuthError(
					$response->error,
					$status_code,
					$response->error_description
				);
			} else {
				throw new FireApiClient_ApiError(
					$response->message,
					$status_code
				);
			}
		}
		curl_close($curl);

		$response = $this->last_response['body'];

		if ( $this->decode_json ) {
			$response = json_decode($response);
		}

		return $response;
	}

	/**
	 * Do a GET request.
	 *
	 * @param $url string
	 * @return stdClass
	 */
	public function get($url)
	{
		return $this->request('GET', $url);
	}

	/**
	 * Do a POST request
	 *
	 * @param $url string
	 * @param $data array
	 * @return stdClass
	 */
	public function post($url, array $data = array())
	{
		return $this->request('POST', $url, $data);
	}

	/**
	 * Do a POST request with file upload
	 *
	 * @param $url string
	 * @param $data array
	 * @return stdClass
	 */
	public function upload($url, array $data = array())
	{
		return $this->request('POST', $url, $data, array(
			'multipart' => true
		));
	}

	/**
	 * Do a PUT request
	 *
	 * @param $url string
	 * @param $data array
	 * @return stdClass
	 */
	public function put($url, array $data = array())
	{
		return $this->request('PUT', $url, $data);
	}

	/**
	 * Do a DELETE request
	 *
	 * @param $url string
	 * @return stdClass
	 */
	public function delete($url)
	{
		return $this->request('DELETE', $url);
	}

	/**
	 * Get last response body
	 *
	 * @return string
	 */
	public function getLastBody()
	{
		return $this->last_response['body'];
	}

	/**
	 * Get last request
	 *
	 * @return string
	 */
	public function getLastRequest()
	{
		return $this->last_request;
	}

	/**
	 * Get last response status
	 *
	 * @return int
	 */
	public function getLastStatus()
	{
		return $this->last_response['meta']['http_code'];
	}

	/**
	 * Get last headers
	 *
	 * @return array
	 */
	public function getLastHeaders()
	{
		return $this->last_headers;
	}

	/**
	 * Get last URL called by the API.
	 *
	 * @return string
	 */
	public function getLastUrl()
	{
		return $this->last_response['meta']['url'];
	}

	/**
	 * Return the entire "meta" array provided by cURL.
	 *
	 * @return array
	 */
	public function getLastMeta()
	{
		return $this->last_response['meta'];
	}

	/**
	 * Return the last response header (case insensitive) or NULL if not present.
	 * HTTP allows empty headers (e.g. RFC 2616, Section 14.23), thus is_null()
	 * and not negation or empty() should be used.
	 *
	 * @param string $header
	 * @return string
	 */
	public function getLastHeader($header)
	{
		if (empty($this->last_headers[strtolower($header)])) {
			return NULL;
		}
		return $this->last_headers[strtolower($header)];
	}

	/**
	 * Handle header. Callback for cURL.
	 *
	 * @param  $ch
	 * @param  $str
	 * @return int
	 */
	private function processHeader($ch, $str)
	{
		if ( preg_match('/([^:]+):\s(.+)/m', $str, $match) ) {
			$this->last_headers[strtolower($match[1])] = trim($match[2]);
		}
		return strlen($str);
	}
}

/**
 * Exception for Client error (i.e. the client didn't work)
 */
class FireApiClient_Exception extends Exception
{}

/**
 * Exception for API errors (i.e the API returns an error code)
 */
class FireApiClient_ApiError extends FireApiClient_Exception
{
}

/**
 * Exception for Oauth errors (i.e the API returns an error code)
 */
class FireApiClient_OAuthError extends FireApiClient_ApiError
{
	protected $description = '';
	public function __construct($message, $code, $description, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->description = $description;
	}

	/**
	 * Return error description
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
}

/**
 * Specific class for 422 (unprocessable entity) errors, aka validation errors
 * This exception provides an additional getErrors() method.
 */
class FireApiClient_ValidationError extends FireApiClient_ApiError
{
	/**
	 * @var array
	 */
	protected $errors = array();
	public function __construct($message, $code, $errors, Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->errors = $errors;
	}

	/**
	 * Return error array
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}
}